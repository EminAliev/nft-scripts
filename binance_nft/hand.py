import sys
import threading
import time

from datetime import datetime, timedelta

import requests

from binance_nft.const import COUNT_REQUESTS, PROXY, headers

from binance_nft.captcha import Recaptcha


def event_is_not_over(status: int) -> bool:
    return status == 0

def headers_is_right() -> bool:
    user_info = 'https://www.binance.com/bapi/accounts/v1/private/account/user/base-detail'
    response = requests.post(user_info, headers=headers)

    if response.status_code == 200:
        print('Все чётко\n')
    else:
        print('Чото пошло не так, чекай куки и всю хуйню...')
        sys.exit(1)

def send_requests_to_buy(box, start_sale_time: datetime, product_id: str):
    threads = list()
    captcha = Recaptcha(product_id)

    while True:
        current_time = datetime.today()
	if start_sale_time <= (current_time + timedelta(seconds=105)):
            print('Подбираем капчу')
            captcha_list = captcha.prepare_captcha()
            print('Все четко, капча сломана епта')
            break
     

    while True:
        current_time = datetime.today()
        if start_sale_time <= (current_time + timedelta(seconds=1.5)):
            print('Пошла закупка, жду процент')
            for _ in range(0, COUNT_REQUESTS):
                request = threading.Thread(
                    target=box._buy_box,
                    args=(PROXY)
                )
                request.start()
                threads.append(request)
                time.sleep(0.15)

            for thread in threads:
                thread.join()

            print('Все закончили, папане скидываем')
            sys.exit(0)
